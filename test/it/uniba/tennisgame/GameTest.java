package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

//SCORE test
	
	@Test
		public void testLoveFifteen() throws Exception{
		//Arrange 
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player2.getName());
		//Assert
		assertEquals("Federer love - Nadal fifteen",game.getGameStatus());
		
	}
	
	@Test
	public void testFortyFifteen() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		//Assert
		assertEquals("Federer forty - Nadal fifteen", game.getGameStatus());
		
	}
	
	@Test // wins player1 ???
	public void testPlayer1wins() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player1.getName()); //solleva l'eccezione perch� il giocatore 1 aveva gi� vinto -> conta l'ordine con cui incrementi => incrementiamo prima due volte il primo giocatore, poi il secondo e poi di nuovo il primo
		game.incrementPlayerScore(player1.getName()); 
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player1.getName());
		//game.incrementPlayerScore(player1.getName());  game already be won, ovvero il giocatore 1 ha gi� vinto, aveva gi� accumulato due punti in pi� dell'avversario

		//Assert
		assertEquals("Federer wins", game.getGameStatus());
	}
	
	@Test
	public void testLoveForty() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		//Assert
		assertEquals("Federer love - Nadal forty", game.getGameStatus());
		
	}
	
	@Test
	public void testDeuce() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		//Assert
		assertEquals("Deuce", game.getGameStatus());
		
	}
	
	@Test
	public void testFedererWins() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		 /* li metto in pareggio e poi in vantaggio */
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		
		//Assert
		assertEquals("Federer wins",game.getGameStatus());
	}
	
	@Test
	public void testNadalWins() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());		
		//Assert
		assertEquals("Nadal wins",game.getGameStatus());
	}
	
	@Test
	public void advantageNadal() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		//Assert
		assertEquals("Advantage Nadal",game.getGameStatus());
	}
	
	@Test
	public void advantageFederer() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		Player player1 = new Player("Federer",0);
		Player player2 = new Player("Nadal",0);
		//Act
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player1.getName());
		
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player2.getName());
		
		
		game.incrementPlayerScore(player1.getName());
		game.incrementPlayerScore(player2.getName());
		game.incrementPlayerScore(player1.getName());

		
		//Assert
		assertEquals("Advantage Federer", game.getGameStatus());
	}
	

}//FINE CLASSE
