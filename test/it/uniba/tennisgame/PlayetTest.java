package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

//innanzitutto dobbiamo decidere che tecnica per disegnare i test vogliamo utilizzare e come possiamo applicarla al caso 
//supponiamo di voler applicare il BoundaryValueTesting -> valore nominale dell'input (il numero di chiamate a IncrementScore) ecc. , l'output (la stringa che viene restituita) NON LO SCEGLIAMO
//se invece volessimo applicare l' EquivalenceClassTesting -> love ... ; identifico in maniera banale due classi di equivalenza : quella per cui lo score viene incrementato almeno una volta e quella in cui neanche una volta 

public class PlayetTest {

//INPUT OUTPUT
	
	@Test //altrimenti non verr� mai riconosciuto come metodo da JUnit e non verr� mai eseguito
/*i*/  public void scoreShouldBeIncreased() {
		//Arrange 
		Player player1 = new Player("Federer",0);
		//Act
		player1.incrementScore();
		//Assert
		assertEquals(1,player1.getScore());
	}
	
	@Test
/*ni*/  public void scoreShouldNotBeIncreased() {
		//Arrange 
		Player player1 = new Player("Federer",0);
		//Assert
		assertEquals(0,player1.getScore());
	}

	
//CLASSI VALIDE	
	
	@Test
/*love*/ public void scoreShouldBeLove(){
 	//Arrange 
	Player player1 = new Player("Federer",0); 
	//Act
	String scoreAsString = player1.getScoreAsString();
	//Assert
	assertEquals("love", scoreAsString);  
 	}  
	
	@Test
/*fifteen*/ public void scoreShouldBeFifteen(){
 	//Arrange 
	Player player1 = new Player("Federer",1);
	//Act
	String scoreAsString = player1.getScoreAsString();
	//Assert
	assertEquals("fifteen", scoreAsString);  
 	}  
	
	@Test
/*thirty*/ public void scoreShouldBeThirty(){
 	//Arrange 
	Player player1 = new Player("Federer",2);
	//Act
	String scoreAsString = player1.getScoreAsString();
	//Assert
	assertEquals("thirty", scoreAsString);  
 	}  
	
	@Test
/*forty*/ public void scoreShouldBeForty(){
 	//Arrange 
	Player player1 = new Player("Federer",3);
	//Act
	String scoreAsString = player1.getScoreAsString();
	//Assert
	assertEquals("forty", scoreAsString);  
 	}  
	
	
//CLASSI NON VALIDE	
	
	@Test
/*nullIfNegative*/ public void scoreShouldNullIfNegative(){
 	//Arrange 
	Player player1 = new Player("Federer",-1);
	//Act
	String scoreAsString = player1.getScoreAsString();
	//Assert
	assertNull(scoreAsString);  //!!
 	}  
	
	@Test
/*moreThanThree*/ public void scoreShouldMoreThanThree(){
 	//Arrange 
	Player player1 = new Player("Federer",4);
	//Act
	String scoreAsString = player1.getScoreAsString();
	//Assert
	assertNull(scoreAsString);  
 	}  

	
//PAREGGIO e NON PAREGGIO
	
	@Test
/*BeTie*/ public void shouldBeTie(){
 	//Arrange 
	Player player1 = new Player("Federer",2);
	Player player2 = new Player("Nadal",2);
	//Act
	boolean tie = player1.isTieWith(player2); //!!
	//Assert
	assertTrue(tie); //!!
 	}  

	@Test
/*NotBeTie*/ public void shouldNotBeTie(){
 	//Arrange 
	Player player1 = new Player("Federer",3);
	Player player2 = new Player("Nadal",2);
	//Act
	boolean tie = player1.isTieWith(player2); 
	//Assert
	assertFalse(tie); //!!
 	}  

	
//ALMENO 4 PUNTI E NON ALMENO 4 PUNTI
	
	@Test
/*fortypoints*/ public void shouldHaveAtLeastFortyPoints(){
 	//Arrange 
	Player player1 = new Player("Federer",3);
	//Act
	boolean outcome = player1.hasAtLeastFortyPoints(); //!!
	//Assert
	assertTrue(outcome);
 	}  
	
	@Test
/*notfortypoints*/ public void shouldNotHaveAtLeastFortyPoints(){
	 	//Arrange 
		Player player1 = new Player("Federer",2);
		//Act
		boolean outcome = player1.hasAtLeastFortyPoints(); //!!
		//Assert
		assertFalse(outcome);
	 	}  


//MENO DI 3 PUNTI E PIU' DI 3 PUNTI

	@Test
/*lessfortypoints*/ public void shouldHaveLessThanFortyPoints(){
 	//Arrange 
	Player player1 = new Player("Federer",2);
	//Act
	boolean outcome = player1.hasLessThanFortyPoints(); 
	//Assert
	assertTrue(outcome);
 	}  

	@Test
/*notlessfortypoints*/ public void shouldNotHaveLessThanFortyPoints(){
 	//Arrange 
	Player player1 = new Player("Federer",3);
	//Act
	boolean outcome = player1.hasLessThanFortyPoints(); 
	//Assert
	assertFalse(outcome);
 	} 


//PIU' DI 4 PUNTI E NON PIU' DI 4 PUNTI

	@Test
/*morefourtypoints*/ public void shouldHaveMoreThanFourtyPoints(){
	//Arrange 
	Player player1 = new Player("Federer",4);
	//Act
	boolean outcome = player1.hasMoreThanFourtyPoints(); 
	//Assert
	assertTrue(outcome);
	} 

	@Test
/*notmorefourtypoints*/ public void shouldNotHaveMoreThanFourtyPoints(){
	//Arrange 
	Player player1 = new Player("Federer",3);
	//Act
	boolean outcome = player1.hasMoreThanFourtyPoints(); 
	//Assert
	assertFalse(outcome);
	}  

	
//UN PUNTO DI VANTAGGIO E NON 
	
	@Test
/*advantage*/ public void shouldHaveOnePointAdvantageOn(){
	//Arrange 
	Player player1 = new Player("Federer",4);
	Player player2 = new Player("Nadal",3);
	//Act
	boolean outcome = player1.hasOnePointAdvantageOn(player2); 
	//Assert
	assertTrue(outcome);
	} 

/*notadvantage*/ public void shouldNotHaveOnePointAdvantageOn(){
	//Arrange 
	Player player1 = new Player("Federer",3);
	Player player2 = new Player("Nadal",3);
	//Act
	boolean outcome = player1.hasOnePointAdvantageOn(player2); 
	//Assert
	assertFalse(outcome);
	}  


//DUE PUNTI DI VANTAGGIO E NON 

	@Test
/*advantage*/ public void shouldHaveAtLeastTwoPointsAdvantageOn(){
	//Arrange 
	Player player1 = new Player("Federer",5);
	Player player2 = new Player("Nadal",3);
	//Act
	boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2); 
	//Assert
	assertTrue(outcome);
	} 

/*notadvantage*/ public void shouldNotHaveAtLeastTwoPointsAdvantageOn(){
	//Arrange 
	Player player1 = new Player("Federer",4);
	Player player2 = new Player("Nadal",3);
	//Act
	boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2); 
	//Assert
	assertFalse(outcome);
	}  

}
